<?php

declare(strict_types=1);

namespace twinscom\GuzzleComponents\Tests;

use PHPUnit\Framework\TestCase;
use twinscom\GuzzleComponents\BackoffRetryDelay;

/**
 * @SuppressWarnings(PHPMD.StaticAccess)
 *
 * @internal
 */
final class BackoffRetryDelayTest extends TestCase
{
    public function testDefault(): void
    {
        $delay = BackoffRetryDelay::make();

        self::assertSame(
            1,
            $delay(1)
        );

        self::assertSame(
            2,
            $delay(2)
        );

        self::assertSame(
            4,
            $delay(3)
        );
    }

    public function testMultiplier(): void
    {
        $delay = BackoffRetryDelay::make(1000);

        self::assertSame(
            1000,
            $delay(1)
        );

        self::assertSame(
            2000,
            $delay(2)
        );

        self::assertSame(
            4000,
            $delay(3)
        );
    }
}
