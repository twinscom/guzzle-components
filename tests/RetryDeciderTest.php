<?php

declare(strict_types=1);

namespace twinscom\GuzzleComponents\Tests;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use twinscom\GuzzleComponents\RetryDecider;

/**
 * @SuppressWarnings(PHPMD.StaticAccess)
 *
 * @internal
 */
final class RetryDeciderTest extends TestCase
{
    public function testException(): void
    {
        $decide = RetryDecider::make();

        $result = $decide(
            1,
            null,
            null,
            new RequestException(
                'Message',
                new Request('GET', '')
            )
        );

        self::assertTrue($result);
    }

    public function testMaxRetries(): void
    {
        $decide = RetryDecider::make(1);

        $lessThanMaxRetries = $decide(
            0,
            null,
            null,
            new RequestException(
                'Message',
                new Request(
                    'GET',
                    ''
                )
            )
        );

        self::assertTrue($lessThanMaxRetries);

        $equalToMaxRetries = $decide(
            1,
            null,
            null,
            new RequestException(
                'Message',
                new Request(
                    'GET',
                    ''
                )
            )
        );

        self::assertFalse($equalToMaxRetries);
    }

    public function testDefaultMaxRetries(): void
    {
        $decide = RetryDecider::make();

        $lessThanMaxRetries = $decide(
            4,
            null,
            null,
            new RequestException(
                'Message',
                new Request(
                    'GET',
                    ''
                )
            )
        );

        self::assertTrue($lessThanMaxRetries);

        $equalToMaxRetries = $decide(
            5,
            null,
            null,
            new RequestException(
                'Message',
                new Request(
                    'GET',
                    ''
                )
            )
        );

        self::assertFalse($equalToMaxRetries);
    }

    public function testClientError(): void
    {
        $decide = RetryDecider::make();

        $result400 = $decide(
            1,
            null,
            new Response(400),
            null
        );

        self::assertFalse($result400);

        $result499 = $decide(
            1,
            null,
            new Response(499),
            null
        );

        self::assertFalse($result499);
    }

    public function testServerError(): void
    {
        $decide = RetryDecider::make();

        $result500 = $decide(
            1,
            null,
            new Response(500),
            null
        );

        self::assertTrue($result500);

        $result599 = $decide(
            1,
            null,
            new Response(599),
            null
        );

        self::assertTrue($result599);
    }

    public function testOnFulfilled(): void
    {
        $decide = RetryDecider::make();

        $result = $decide(
            1,
            null,
            null,
            null
        );

        self::assertFalse($result);
    }

    public function testClientException(): void
    {
        $decide = RetryDecider::make();

        $result400 = $decide(
            1,
            null,
            new Response(400),
            new ClientException(
                'Message',
                new Request(
                    'GET',
                    ''
                )
            )
        );

        self::assertFalse($result400);
    }
}
