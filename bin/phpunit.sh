#!/bin/sh

set -eu

vendor/phpunit/phpunit/phpunit \
    --bootstrap=tests/bootstrap.php \
    --color=always \
    --strict-global-state \
    --disallow-test-output \
    --coverage-text \
    --coverage-html coverage/ \
    "$@"
