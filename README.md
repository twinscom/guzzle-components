# Guzzle components

[![build status](https://gitlab.com/twinscom/guzzle-components/badges/master/build.svg)](https://gitlab.com/twinscom/guzzle-components/builds)
[![coverage report](https://gitlab.com/twinscom/guzzle-components/badges/master/coverage.svg)](https://gitlab.com/twinscom/guzzle-components/builds)

## Development

### Install

```sh
docker-compose run --rm php composer install
```

### Test

```sh
docker-compose run --rm php bin/test.sh
```

### Fix the code with PHP-CS-Fixer

```sh
docker-compose run --rm php bin/php-cs-fixer/fix.sh
```

## License

[MIT](LICENSE)
