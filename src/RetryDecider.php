<?php

declare(strict_types=1);

namespace twinscom\GuzzleComponents;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class RetryDecider
{
    /**
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public static function make(int $maxRetries = 5): callable
    {
        return static function (
            $retries,
            // phpcs:ignore SlevomatCodingStandard.Functions.UnusedParameter
            Request $ignore = null,
            Response $response = null,
            $exception = null
        ) use ($maxRetries): bool {
            return $retries < $maxRetries && (
                $response
                    ? $response->getStatusCode() >= 500
                    : $exception !== null
            );
        };
    }
}
