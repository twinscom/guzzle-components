<?php

declare(strict_types=1);

namespace twinscom\GuzzleComponents;

class BackoffRetryDelay
{
    public static function make($multiplier = 1): callable
    {
        return static function ($retries) use ($multiplier): int {
            return 2 ** ($retries - 1) * $multiplier;
        };
    }
}
